/**
 * Temperature stores a temperature in Fahrenheit, Celsius,
 * and Kelvin.
 *
 * @author Hasserd
 * @version 4.0
 */

public class Temperature {

	private double degreesFahrenheit;
	private double degreesCelsius;
	private double degreesKelvin;

/**
 * This constructor for Temperature sets the temperature valuses
 * to the values in degrees, based on type
 *
 * @param type temperature scale to use
 * @param degrees degrees Fahrenheit
 */
	public Temperature (String type, double degrees) {
		if (type.equalsIgnoreCase("C"))
			setDegreesCelsius(degrees);
		else if (type.equalsIgnoreCase("F"))
			setDegreesFahrenheit(degrees);
		else if (type.equalsIgnoreCase("K"))
			setDegreesKelvin(degrees);
	}

	public static boolean isTypeValid(String temperatureType) {

		if (temperatureType.equalsIgnoreCase("C") ||
				temperatureType.equalsIgnoreCase("F") ||
				temperatureType.equalsIgnoreCase("K"))
			return true;
		else
			return false;
	}
	
	public static boolean isTemperatureValid(String temperatureType, double temperature) {
		if ((temperatureType.equalsIgnoreCase("C") && temperature >= -273.15) ||
				(temperatureType.equalsIgnoreCase("F") && temperature >= -459.67) ||
				(temperatureType.equalsIgnoreCase("K") && temperature >= 0.0))
				return true;
		else
			return false;
	}

		/**
		 * setDegreesFahrenheit method sets degrees in F
		 *
		 * @param degrees the F value to store
		 */

	public void setDegreesFahrenheit (double degrees) {
		degreesFahrenheit = degrees;
		degreesCelsius = (degreesFahrenheit - 32.0) * 5.0 /9.0;
		degreesKelvin = degreesCelsius + 273.15;

	}

	public void setDegreesCelsius (double degrees) {
		degreesCelsius = degrees;
		degreesFahrenheit = degreesCelsius * 9.0 / 5.0 + 32.0;
		degreesKelvin = degreesCelsius + 273.15;
	}

	public void setDegreesKelvin (double degrees) {
		degreesKelvin = degrees;
		degreesCelsius = degreesKelvin - 273.15;
		degreesFahrenheit = degreesCelsius * 9.0 /5.0 + 32.0;
	}

	
	/**
	 * getDegreesCelsius retrieves the Celsius temperature value
	 *
	 * @return a double value containing temp in C
	 */

	public double getDegreesCelsius() {
		return degreesCelsius;
	}

	/**
	 * getDegreesKelvin retrieves the temp in K
	 *
	 * @return a double value containing temp in K
	 */

	public double getDegreesKelvin() {
		return degreesKelvin;
	}

	public double getDegreesFahrenheit() {
		return degreesFahrenheit;
	}


}
